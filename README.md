# Hackathon Virtuel Inter-cati 2020
![image](img/singularity.png)![image](img/snakemake.png)![image](img/nextflow2014.png)

---

Ce site pointe tous les éléments nécessaires pour participer au hackathon inter-cati du 04/11/2020.
Il a aussi pour vocation de pointer des ressources pour faire fonctionner 
les différents systèmes présentés sur les différentes infrastructures 
bioinformatique de l'INRAE.

lien vers le GITLAB PAGES : [https://inter_cati_omics.pages.mia.inra.fr/reproductibility/](https://inter_cati_omics.pages.mia.inra.fr/reproductibility/)


{% pdf title="Slides d'introduction", src="Hackathon_inter-CATI_Reproductibilite_intro_20201104.pdf", width="100%", height="550", link=false %}{% endpdf %}

## Programme
---

* 9h00 - 9h30  : Introduction (url de connexion : http://genobbb.inrae.fr/b/bio-jtm-nas-s4l)
* 9h30 - 9h45  : Questions et points de blocages
* 9h45 - 12h00 : Singularity (url de connexion : http://genobbb.inrae.fr/b/bio-jtm-nas-s4l)

* 14h-16h45 : Sessions parallèles
 * Nextflow (url de connexion: http://genobbb.inrae.fr/b/bio-ffn-vlt-cyg)
 * Snakemake (url de: http://genobbb.inrae.fr/b/bio-9ec-k2a-ddw)
 
* 16h45-17h : Retours sur la journée, échanges et conclusion

## Pads pour prise de notes durant les ateliers

Singularity: https://mensuel.framapad.org/p/intercati_visio_singularity-9jvq

Nextflow:    https://mensuel.framapad.org/p/intercati_visio_nextflow-9jvq

Snakemake:   https://mensuel.framapad.org/p/intercati_visio_snakemake-9jvq



## Pré-requis
---

Pour participer aux différents ateliers vous aurez besoin :

* __Connexion ssh__

* Pour __réaliser les tutoriels snakemake ou nextflow vous devez vous 
connecter sur les serveurs de la plateforme genotoul__. Si vous n'avez 
pas de compte veuillez faire une demande au support 
(http://bioinfo.genotoul.fr/index.php/ask-for/support/) 
en indiquant "Compte formation pour le hackathon inter-cati"
``` bash
ssh <mylogin>@genologin.toulouse.inra.fr
```


* __Pour réaliser le tutoriel Singularity__ vous devez avoir un __[compte sur 
la forge mia](https://forgemia.inra.fr/)__ pour pouvoir construire des 
images singularity ou une machine Linux (basée sur une distribution debian) avec accès en tant que root: soit linux natif ou sur une vm virtualBox: https://www.virtualbox.org/wiki/Downloads ([tuto](https://cdiese.fr/installation-de-debian-sur-une-machine-virtuelle-virtualbox/)).

## En amont et pendant le hackathon
---
En amont du hackathon, vous pouvez suivre les tutoriels pointés dans les différentes sections.
Durant le hackathon, vous pourrez : 
* travailler sur ces tutoriels 
* ou bien travailler sur vos propres workflows en autonomie

Nous serons à votre disposition pour répondre à vos questions.

Des "sous-salles" virtuelles pourront être créer pour que vous travailliez 
en sous groupes sur un sujet.

## Contacts /Organisateurs :
---
N'hésitez pas à nous contacter en cas de problèmes.
* anthony.bretaudeau@inrae.fr 
* celine.noirot@inrae.fr
* Jacques.lagnel@inrae.fr 
* joseph.tran@inrae.fr 
* maxime.manno@inrae.fr 



