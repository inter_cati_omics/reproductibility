# Singularity ![image](../img/singularity.png)
---

Bienvenue à l'atelier Singularity, notre but est de vous donner un aperçu de ce qu'il est possible de faire avec les images [Singularity](https://singularityhub.github.io/singularityhub-docs/).  
Vous pourrez découvrir comment créer une image, la lancer simplement et aller un peu plus loin dans son utilisation.    

Suivant votre avancée dans l'utilisation de Singularity, vous pourrez naviger sur les différents TPs proposés.  

## pré-requis :  
Etant donné qu'il est nécessaire d'avoir des droits root pour pouvoir créer une image Singularity, vous aurez besoin d'une machine linux ou bien d'une machine virtuelle. Voici un tutoriel pour installer une [machine virtuelle Debian](https://cdiese.fr/installation-de-debian-sur-une-machine-virtuelle-virtualbox/).    

Tous les fichiers mentionnés dans les TP sont disponibles sur le Gitlab :   
[https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/tree/master/singularity](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/tree/master/singularity)

Vous pouvez également en faire un clone sur votre espace de travail :  
```bash
git clone https://forgemia.inra.fr/inter_cati_omics/reproductibility.git
```



## @Ceux qui n'ont pas encore installé Singularity  
Vous pouvez retrouver un TP qui explique comment __installer Singularity__ à partir d'une __machine linux__ ou bien d'une __VM__.  
Lien vers le TP1 [Singularity_TP1_installation.adoc](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/blob/master/singularity/Singularity_TP1_installation.adoc).
sources: [TP1](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/tree/master/singularity/TP1) 

## @Ceux qui n'ont jamais utilisé d'images Singularity
Voici un tutoriel pour apprendre a __créer et lancer des images Singularity__.  
__! Attention__, ce ne sera pas possible sur Genotoul car vous devez être root.  
Lien vers le TP2 [Singularity_tp2_first_container.adoc](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/blob/master/singularity/Singularity_TP2_first_container.adoc). Sources: [TP2_first_container](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/tree/master/singularity/TP2_first_container) 


## @Ceux qui souhaitent se renseigner sur les bonnes pratiques peuvent se diriger vers ce TP :  
Voici un TP sur les __bonnes pratiques__ pour les utilisateurs et administrateurs.  
Lien vers le TP3 [Singularity_TP3_good_practices.adoc](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/blob/master/singularity/Singularity_TP3_good_practices.adoc). Sources: [TP3_CICD](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/tree/master/singularity/TP3_CICD) 

## @Ceux qui souhaitent apprendre à créer un image via la Forgemia, sans passer par une machine Linux
Pour ceux qui travaillent sur Génotoul par exemple ou __n'ont pas de machine Linux__, il existe un moyen de créer des images Singularity via la __Forgemia__.  
Ce TP vous permettra d'en apprendre plus et de commencer à utiliser cet outil.  
Lien vers le TP3 [Singularity_TP3_good_practices.adoc](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/blob/master/singularity/Singularity_TP3_good_practices.adoc). Sources: [TP3_CICD](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/tree/master/singularity/TP3_CICD) 

## @Ceux qui souhaitent apprendre à créer un image pour executer du code MPI  
Ce TP vous permettra d'en apprendre plus et de commencer à utiliser cet outil.  
Lien vers le TP4 [Singularity_TP4_mpi.adoc](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/blob/master/singularity/Singularity_TP4_mpi.adoc).  Sources: [TP4_mpi](https://forgemia.inra.fr/inter_cati_omics/reproductibility/-/tree/master/singularity/TP4_mpi)
## Liens :

* Voici un tutoriel officiel pour démarrer avec singularity [https://sylabs.io/guides/3.5/user-guide/quick_start.html](https://sylabs.io/guides/3.5/user-guide/quick_start.html)
* Documentation Singularity [https://singularityhub.github.io/singularityhub-docs/](https://singularityhub.github.io/singularityhub-docs/)
* Lien vers les dépos d'images Singularity des packages conda [https://depot.galaxyproject.org/singularity/](https://depot.galaxyproject.org/singularity)
* Lien vers exemples de recettes et d'images Singularity sur la forgemia (utilisation CI/CD) [https://forgemia.inra.fr/gafl/singularity](https://forgemia.inra.fr/gafl/singularity) 

